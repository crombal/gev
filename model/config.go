package model

import (
	"fmt"
	"os"
)

type Config struct {
	Token     string
	ProjectID string
	Domain    string
	ApiUrl    string
}

func NewConfig() (*Config, error) {
	token := os.Getenv("TOKEN")
	projectID := os.Getenv("PROJECT_ID")
	domain := os.Getenv("DOMAIN")
	if token == "" || projectID == "" || domain == "" {
		return nil, fmt.Errorf("TOKEN, PROJECT_ID or DOMAIN environment variable is not set")
	}
	return &Config{
		Token:     token,
		ProjectID: projectID,
		Domain:    domain,
		ApiUrl:    fmt.Sprintf("https://%s/api/v4/projects/%s/variables", domain, projectID),
	}, nil
}
