package model

import (
	"fmt"
)

type Response struct {
	VariableType     string `json:"variable_type"`
	Key              string `json:"key"`
	Value            string `json:"value"`
	Protected        bool   `json:"protected"`
	Masked           bool   `json:"masked"`
	Raw              bool   `json:"raw"`
	EnvironmentScope string `json:"environment_scope"`
}

func (response Response) TextOutput() string {
	return fmt.Sprintf(
		"VariableType: %s\nKey: %s\nValue: %s\nProtected: %t\nMasked: %t\nRaw: %t\nEnvironmentScope: %s\n\n",
		response.VariableType,
		response.Key,
		response.Value,
		response.Protected,
		response.Masked,
		response.Raw,
		response.EnvironmentScope,
	)
}
