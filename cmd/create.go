package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/crombal/gev/model"
)

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new variable",
	Long: `Create a new variable.
If a variable with the same key already exists, the new variable must have a different environment_scope.
Otherwise, GitLab returns a message similar to: VARIABLE_NAME has already been taken.`,
	Run: createRun,
}

func createRun(cmd *cobra.Command, args []string) {
	if err := validateCreateInput(); err != nil {
		log.Fatalln(err)
	}

	cfg, err := model.NewConfig()
	if err != nil {
		log.Fatalln(err)
	}

	formData := createFormData()
	req, err := createRequest(cfg, formData)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err, "Error making request")
	}
	defer resp.Body.Close()

	handleCreateResponse(resp)
}

func createFormData() url.Values {
	return url.Values{
		"variable_type":     {variableType},
		"key":               {key},
		"value":             {value},
		"protected":         {strconv.FormatBool(protected)},
		"masked":            {strconv.FormatBool(masked)},
		"raw":               {strconv.FormatBool(raw)},
		"environment_scope": {environmentScope},
	}
}

func createRequest(cfg *model.Config, formData url.Values) (*http.Request, error) {
	req, err := http.NewRequest("POST", cfg.ApiUrl, strings.NewReader(formData.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Set("PRIVATE-TOKEN", cfg.Token)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return req, nil
}

func handleCreateResponse(resp *http.Response) {
	var varResponse model.Response
	if err := json.NewDecoder(resp.Body).Decode(&varResponse); err != nil || resp.StatusCode != http.StatusCreated {
		log.Fatal("Ooops! An error occurred, please try again.")
	}

	log.Printf(varResponse.TextOutput())
}

func validateCreateInput() error {
	if key == "" {
		return errors.New("the key argument is required")
	}
	if len(key) > 255 {
		return errors.New("the key argument must have no more than 255 characters")
	}
	if !validKeyRegex(key) {
		return fmt.Errorf("key '%s' is not in valid format, must be between 1 and 255 characters long and only contain characters from A-Z, a-z, 0-9, and _", key)
	}
	if value == "" {
		return errors.New("the value argument is required")
	}
	if variableType != "env_var" && variableType != "file" {
		return errors.New("the type argument must be 'env_var' or 'file'")
	}

	return nil
}

func validKeyRegex(key string) bool {
	validKey := regexp.MustCompile(`^[A-Za-z0-9_]{1,255}$`)

	return validKey.MatchString(key)
}

func init() {
	createCmd.PersistentFlags().StringVarP(&key, "key", "k", "", "The key of a variable; must have no more than 255 characters; only A-Z, a-z, 0-9, and _ are allowed")
	createCmd.MarkPersistentFlagRequired("key")
	createCmd.PersistentFlags().StringVarP(&value, "value", "v", "", "The value of a variable")
	createCmd.MarkPersistentFlagRequired("value")
	createCmd.PersistentFlags().StringVarP(&variableType, "type", "t", "env_var", "The type of a variable. Available types are: env_var and file")
	createCmd.PersistentFlags().BoolVarP(&protected, "protected", "p", false, "Whether the variable is protected")
	createCmd.PersistentFlags().BoolVarP(&masked, "masked", "m", false, "Whether the variable is masked")
	createCmd.PersistentFlags().BoolVarP(&raw, "raw", "r", false, "Whether the variable is expandable")
	createCmd.PersistentFlags().StringVarP(&environmentScope, "scope", "s", "*", "The environment_scope of the variable")
	rootCmd.AddCommand(createCmd)
}
