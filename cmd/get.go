package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/crombal/gev/model"
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get the details of a single variable",
	Long: `Get the details of a single variable.
If there are multiple variables with the same key, use filter to select the correct environment_scope.`,
	Run: getRun,
}

func getRun(cmd *cobra.Command, args []string) {
	cfg, err := model.NewConfig()
	if err != nil {
		log.Fatalln(err)
	}

	req, err := createGetRequest(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	handleGetResponse(resp)
}

func createGetRequest(cfg *model.Config) (*http.Request, error) {
	url := fmt.Sprintf("%s/%s?filter[environment_scope]=%s", cfg.ApiUrl, key, filter)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	return req, nil
}

func handleGetResponse(resp *http.Response) {
	var varResponse model.Response
	if err := json.NewDecoder(resp.Body).Decode(&varResponse); err != nil || resp.StatusCode != http.StatusOK {
		log.Fatal("Ooopsss! An error occurred, please try again.")
	}

	log.Printf(varResponse.TextOutput())
}

func init() {
	getCmd.PersistentFlags().StringVarP(&key, "key", "k", "", "The key of a variable")
	getCmd.MarkPersistentFlagRequired("key")
	getCmd.PersistentFlags().StringVarP(&filter, "filter", "f", "*", "Filter for environment scope. Example: prod")
	rootCmd.AddCommand(getCmd)
}
