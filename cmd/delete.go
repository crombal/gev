package cmd

import (
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/crombal/gev/model"
)

var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a project's variable",
	Long: `Delete a project's variable.
If there are multiple variables with the same key, use filter to select the correct environment_scope.`,
	Run: deleteRun,
}

func deleteRun(cmd *cobra.Command, args []string) {
	cfg, err := model.NewConfig()
	if err != nil {
		log.Fatalln(err)
	}

	client, req, err := deleteClientAndRequest(cfg, key, filter)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	handleDeleteResponse(resp)
}

func deleteClientAndRequest(cfg *model.Config, key, filter string) (*http.Client, *http.Request, error) {
	client := http.Client{}
	url := fmt.Sprintf("%s/%s?filter[environment_scope]=%s", cfg.ApiUrl, key, filter)

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, nil, err
	}

	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	return &client, req, nil
}

func handleDeleteResponse(resp *http.Response) {
	if resp.StatusCode != http.StatusNoContent {
		log.Fatal("Variable with this key not found.")
	}
}

func init() {
	deleteCmd.PersistentFlags().StringVarP(&key, "key", "k", "", "The key of a variable")
	deleteCmd.MarkPersistentFlagRequired("key")
	deleteCmd.PersistentFlags().StringVarP(&filter, "filter", "f", "*", "Filter for environment scope. Example: prod")
	rootCmd.AddCommand(deleteCmd)
}
