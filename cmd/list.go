package cmd

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/crombal/gev/model"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "Get list of a project's variables.",
	Long:  `List will show you all existing env vars in the specified Gitlab project.`,
	Run:   listRun,
}

func listRun(cmd *cobra.Command, args []string) {
	cfg, err := model.NewConfig()
	if err != nil {
		log.Fatalln(err)
	}

	req, err := createListRequest(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	handleListResponse(resp)
}

func createListRequest(cfg *model.Config) (*http.Request, error) {
	req, err := http.NewRequest("GET", cfg.ApiUrl, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	return req, nil
}

func handleListResponse(resp *http.Response) {
	if resp.StatusCode != http.StatusOK {
		log.Fatal("Ooops! An error occurred, please try again.")
	}

	var varsResponse []model.Response
	if err := json.NewDecoder(resp.Body).Decode(&varsResponse); err != nil {
		log.Fatal("Ooops! An error occurred, please try again.")
	}

	for _, varResponse := range varsResponse {
		log.Printf(varResponse.TextOutput())
	}
}

func init() {
	rootCmd.AddCommand(listCmd)
}
