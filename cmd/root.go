package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var key string
var value string
var variableType string
var protected bool
var masked bool
var raw bool
var environmentScope string
var filter string

var rootCmd = &cobra.Command{
	Use:   "gev",
	Short: "Gev is a CLI library for interancting with env vars in diffenent Gitlab projects",
	Long: `Gev will help you to interanct with env vars in diffenent Gitlab projects in less time.
It's designed to be a simple as possible.`,
}

func Execute() {
	log.SetFlags(0)
	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
