package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/crombal/gev/model"
)

var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a project's variable",
	Long: `Update a project's variable.
If there are multiple variables with the same key, use filter to select the correct environment_scope.`,
	Run: updateRun,
}

func updateRun(cmd *cobra.Command, args []string) {
	cfg, err := model.NewConfig()
	if err != nil {
		log.Fatalln(err)
	}

	req, err := createUpdateRequest(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	handleUpdateResponse(resp)
}

func createUpdateRequest(cfg *model.Config) (*http.Request, error) {
	url := fmt.Sprintf(
		"%s/%s?value=%s&protected=%t&masked=%t&raw=%t&environment_scope=%s&filter[environment_scope]=%s",
		cfg.ApiUrl,
		key,
		value,
		protected,
		masked,
		raw,
		environmentScope,
		filter,
	)

	req, err := http.NewRequest("PUT", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	return req, nil
}

func handleUpdateResponse(resp *http.Response) {
	if resp.StatusCode != http.StatusOK {
		log.Fatal("Ooopsss! An error occurred, please try again.")
	}

	var varResponce model.Response
	if err := json.NewDecoder(resp.Body).Decode(&varResponce); err != nil {
		log.Fatal("Ooopsss! An error occurred, please try again.")
	}

	log.Printf(varResponce.TextOutput())
}

func init() {
	updateCmd.PersistentFlags().StringVarP(&key, "key", "k", "", "	The key of a variable")
	updateCmd.MarkPersistentFlagRequired("key")
	updateCmd.PersistentFlags().StringVarP(&value, "value", "v", "", "The value of a variable")
	updateCmd.MarkPersistentFlagRequired("value")
	updateCmd.PersistentFlags().StringVarP(&variableType, "type", "t", "env_var", "The type of a variable. Available types are: env_var and file")
	updateCmd.PersistentFlags().BoolVarP(&protected, "protected", "p", false, "Whether the variable is protected")
	updateCmd.PersistentFlags().BoolVarP(&masked, "masked", "m", false, "Whether the variable is masked")
	updateCmd.PersistentFlags().BoolVarP(&raw, "raw", "r", false, "Whether the variable is expandable")
	updateCmd.PersistentFlags().StringVarP(&environmentScope, "scope", "s", "*", "The environment_scope of the variable")
	updateCmd.PersistentFlags().StringVarP(&filter, "filter", "f", "*", "Filter for environment scope. Example: prod")
	rootCmd.AddCommand(updateCmd)
}
