# Gev - Gitlab Env Vars

Gev is a command line utility that allows you to interact with environment variables in different Gitlab projects. It's designed to be as simple as possible, with a focus on ease of use and efficiency.

With Gev, you can easily:

* List all environment variables in a project
* Add new environment variables to a project
* Update existing environment variables
* Delete environment variables

## Installation

To install Gev, you can either download a pre-built binary from the releases page or build it from source.

### Pre-built Binary

* Download the latest binary for your platform from the releases page
* Make the binary executable chmod +x gev
* Move the binary to a directory on your PATH (e.g. /usr/local/bin)

## Configuration

Before you can use Gev, you will need to configure it with your Gitlab API token, project ID, and domain. You can do this by setting the following environment variables:

* `TOKEN`: Your Gitlab API token
* `PROJECT_ID`: The ID of the project you want to interact with
* `DOMAIN`: The domain of your Gitlab instance (e.g. gitlab.com)

## Usage

```bash
Usage:
  gev [command]

Available Commands:
  add         Add a new environment variable
  delete      Delete an environment variable
  help        Help about any command
  list        List all environment variables
  update      Update an environment variable

Flags:
  -h, --help   help for gev

Use "gev [command] --help" for more information about a command.
```

### Listing Environment Variables

```bash
gev list
```

### Getting a Single Environment Variable

```bash
gev get -k APP_SECRET
```

Please note that protected variables will not be exposed in the output.

### Adding Environment Variables

```bash
gev add \
    --key=KEY \
    --value=VALUE \
    --variable-type=TYPE \
    --protected=false \
    --masked=false \
    --raw=false \
    --environment-scope=SCOPE
```

### Updating Environment Variables

```bash
gev update \
    --key=KEY \
    --value=NEW_VALUE \
    --variable-type=NEW_TYPE \
    --protected=false \
    --masked=false \
    --raw=false \
    --environment-scope=NEW_SCOPE
```

### Deleting Environment Variables

```bash
gev delete --key=KEY
```

## Contributing

If you are interested in contributing to Gev, please feel free to fork the repository and submit pull requests.